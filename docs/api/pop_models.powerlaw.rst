pop_models.powerlaw module
==========================

.. automodule:: pop_models.powerlaw
    :members:
    :undoc-members:
    :show-inheritance:
