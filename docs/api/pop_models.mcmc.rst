pop_models.mcmc module
======================

.. automodule:: pop_models.mcmc
    :members:
    :undoc-members:
    :show-inheritance:
