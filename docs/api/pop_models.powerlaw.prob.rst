pop_models.powerlaw.prob module
===============================

.. automodule:: pop_models.powerlaw.prob
    :members:
    :undoc-members:
    :show-inheritance:
