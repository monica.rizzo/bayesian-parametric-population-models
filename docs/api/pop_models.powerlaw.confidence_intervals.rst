pop_models.powerlaw.confidence_intervals module
===============================================

.. automodule:: pop_models.powerlaw.confidence_intervals
    :members:
    :undoc-members:
    :show-inheritance:
