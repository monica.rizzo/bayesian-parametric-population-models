The pop_models API
==================

:Release: |version|
:Date: |today|

.. toctree::
   :maxdepth: 2

   pop_models.extract_samples
   pop_models.mc
   pop_models.mcmc
   pop_models.powerlaw
   pop_models.powerlaw.confidence_intervals
   pop_models.powerlaw.dist_plots
   pop_models.powerlaw.joint_dist_plot
   pop_models.powerlaw.marginal_plots
   pop_models.powerlaw.mcmc
   pop_models.powerlaw.plot_config
   pop_models.powerlaw.prob
   pop_models.powerlaw.utils
   pop_models.prob
   pop_models.utils
   pop_models.vt
