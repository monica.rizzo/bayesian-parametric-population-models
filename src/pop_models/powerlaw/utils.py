def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()


def load(post_file, param_names, metadata):
    # Load posteriors and constants from posterior file.
    i = 0
    params = {}
    for param_name in param_names:
        # Load in constant
        if param_name in metadata:
            params[param_name] = metadata[param_name]
        # Load in posteriors
        else:
            params[param_name] = post_file["pos"][...,i]
            i += 1
    return params


def loop_constants(iters_and_consts):
    import numpy
    import itertools

    if not isinstance(iters_and_consts, tuple):
        raise TypeError("`iters_and_consts` must be a tuple")

    def loop_if_constant(ic):
        return itertools.repeat(ic) if numpy.isscalar(ic) else ic

    return [
        loop_if_constant(ic)
        for ic in iters_and_consts
    ]


def upcast_scalars(arrays):
    import numpy
    dims = [numpy.ndim(arr) for arr in arrays]
    shape = numpy.shape(arrays[numpy.argmax(dims)])

    if shape == ():
        shape = (1,)

    result = []
    for arr in arrays:
        if numpy.shape(arr) == shape:
            result.append(numpy.asarray(arr))
        else:
            result.append(numpy.tile(arr, shape))

    return result
