from __future__ import division, print_function

alpha_fmt = r"""
\newcommand{{\alphaplrange}}{{\macro{{\ensuremath{{{med:.2f}^{{{plus:+.2f}}}_{{{minus:+.2f}}} }} }} }}
<alpha> +/- sigma_alpha = {mean:.2f} +/- {stdev:.2f}
""".format

rate_fmt = r"""
90% R = {med:d} - {minus} + {plus} Gpc^{{-3}} yr^{{-1}}
90% R in [{lo}, {hi}] Gpc^{{-3}} yr^{{-1}}
""".format

ci_90 = [0.05, 0.50, 0.95]

def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing MCMC samples.",
    )

    parser.add_argument(
        "--rate-logU-to-jeffereys",
        action="store_true",
        help="Assuming rate is log-uniform, switch to Jefferey's prior.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    import h5py
    import numpy

    from . import prob
    from . import utils as pl_utils
    from .. import utils

    if raw_args is None:
        import sys
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    with h5py.File(args.posteriors, "r") as f:
        metadata = f.attrs
        param_names = prob.param_names
        params = pl_utils.load(f, param_names, metadata)

        log10_rate = params["log_rate"]
        alpha = params["alpha"]

        rate = numpy.power(10.0, log10_rate)

        if args.rate_logU_to_jeffereys:
            weights = numpy.sqrt(rate)
        else:
            weights = None

        alpha_lo, alpha_med, alpha_hi = utils.quantile(
            alpha, ci_90, weights=weights,
        )
        alpha_plus = alpha_hi - alpha_med
        alpha_minus = alpha_lo - alpha_med
        alpha_mean = numpy.average(alpha, weights=weights)
        alpha_2nd_moment = numpy.average(numpy.square(alpha), weights=weights)

        n = numpy.size(alpha)
        bessels_correction = n / (n - 1) if n > 1 else numpy.nan
        alpha_variance = bessels_correction * (
            alpha_2nd_moment - numpy.square(alpha_mean)
        )
        alpha_stdev = numpy.sqrt(alpha_variance)

        rate_lo, rate_med, rate_hi = utils.quantile(
            rate, ci_90, weights=weights,
        )
        rate_plus = rate_hi - rate_med
        rate_minus = rate_lo - rate_med

        print(
            alpha_fmt(
                med=alpha_med,
                plus=alpha_plus, minus=alpha_minus,
                mean=alpha_mean, stdev=alpha_stdev,
            )
        )

        print(
            rate_fmt(
                med=int(numpy.round(rate_med)),
                lo=int(numpy.round(rate_lo)), hi=int(numpy.round(rate_hi)),
                plus=int(numpy.round(rate_plus)),
                minus=int(numpy.round(abs(rate_minus))),
            )
        )
