def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing MCMC samples.",
    )
    parser.add_argument(
        "output_fmt",
        help="Format string which evaluates to filename to store plot in. "
             "Should have the string '{param}' in it, which will be replaced "
             "by each free parameter of the MCMC.",
    )

    parser.add_argument(
        "--n-samples",
        default=1000, type=int,
        help="Number of samples to use in each plot.",
    )

    parser.add_argument(
        "--rate-logU-to-jeffereys",
        action="store_true",
        help="Assuming rate is log-uniform, switch to Jefferey's prior.",
    )

    parser.add_argument(
        "--fig-size",
        type=float, default=(8,8), nargs=2,
        help="Dimensions for figures.",
    )
    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib plots.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    if raw_args is None:
        import sys
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    import h5py
    import numpy
    import matplotlib
    matplotlib.use(args.mpl_backend)
    import matplotlib.pyplot as plt
    import seaborn as sns

    matplotlib.rc("text", usetex=True)

    sns.set_context("talk", font_scale=2, rc={"lines.linewidth": 2.5})
    sns.set_style("ticks")
    sns.set_palette("colorblind")

    from . import prob
    from . import plot_config
    from . import utils

    with h5py.File(args.posteriors, "r") as post_file:
        metadata = post_file.attrs
        param_names = prob.param_names
        params = utils.load(post_file, param_names, metadata)


        # Determine sample weights
        if (not args.rate_logU_to_jeffereys) or ("log_rate" in post_file.attrs):
            weights = None
        else:
            weights = numpy.sqrt(numpy.power(10.0, params["log_rate"]))

        # Make plots
        for param_name in param_names:
            # Skip over constants
            if param_name in metadata:
                continue

            fig, ax = plt.subplots(figsize=args.fig_size)

            name = plot_pdf(
                ax,
                params[param_name], weights,
                param_name,
                args.n_samples,
                metadata,
            )

            fig.tight_layout()
            fig.savefig(args.output_fmt.format(param=name))


def plot_pdf(ax, param, weights, param_name, n_samples, metadata):
    return {
        "log_rate": _plot_rate,
        "alpha": _plot_alpha,
        "m_min": _plot_m_min,
        "m_max": _plot_m_max,
    }[param_name](ax, param, weights, n_samples, metadata)


def _plot_rate(ax, log10_rate, weights, n_samples, metadata):
    import numpy
#    from statsmodels.nonparametric.kde import KDEUnivariate
    from ..utils import gaussian_kde

    from . import plot_config

    rate_label = plot_config.labels["rate"]
    rate_units = plot_config.units["rate"]

    log10_rate_smooth = numpy.linspace(
        metadata["log_rate_min"], metadata["log_rate_max"],
        n_samples,
    )
    rate_smooth = numpy.power(10.0, log10_rate_smooth)

#    dP_dlog10_rate = KDEUnivariate(log10_rate)
#    dP_dlog10_rate.fit(kernel="gau", bw="scott", fft=False, weights=weights)
#    dP_dlog10_rate_smooth = dP_dlog10_rate.evaluate(log10_rate_smooth)

    dP_dlog10_rate = gaussian_kde(
        log10_rate, weights=weights,
        bw_method="scott",
    )
    dP_dlog10_rate_smooth = dP_dlog10_rate(log10_rate_smooth)

    dP_drate_smooth = dP_dlog10_rate_smooth / (rate_smooth * numpy.log(10.0))


    ax.semilogx(rate_smooth, rate_smooth*dP_drate_smooth)

    ax.set_xlabel(
        r"${rate_label} [{rate_units}]$"
        .format(rate_label=rate_label, rate_units=rate_units)
    )
    ax.set_ylabel(
        r"${rate_label} p({rate_label})$"
        .format(rate_label=rate_label)
    )

    return "rate"


def _plot_alpha(ax, alpha, weights, n_samples, metadata):
    import numpy
#    from statsmodels.nonparametric.kde import KDEUnivariate
    from ..utils import gaussian_kde

    from . import plot_config

    alpha_label = plot_config.labels["alpha"]

    alpha_smooth = numpy.linspace(
        metadata["alpha_min"], metadata["alpha_max"],
        n_samples,
    )

    # dP_dalpha = KDEUnivariate(alpha)
    # dP_dalpha.fit(kernel="gau", bw="scott", fft=False, weights=weights)
    # dP_dalpha_smooth = dP_dalpha.evaluate(alpha_smooth)

    dP_dalpha = gaussian_kde(
        alpha, weights=weights,
        bw_method="scott",
    )
    dP_dalpha_smooth = dP_dalpha(alpha_smooth)

    ax.plot(alpha_smooth, dP_dalpha_smooth)

    ax.set_xlabel(
        r"${alpha_label}$"
        .format(alpha_label=alpha_label)
    )
    ax.set_ylabel(
        r"$p({alpha_label})$"
        .format(alpha_label=alpha_label)
    )

    return "alpha"


def _plot_m_min(ax, m_min, weights, n_samples, metadata):
    import numpy
#    from statsmodels.nonparametric.kde import KDEUnivariate
    from ..utils import gaussian_kde

    from . import plot_config

    m_min_label = plot_config.labels["m_min"]

    m_min_smooth = numpy.linspace(
        metadata["m_min_min"], metadata["m_min_max"],
        n_samples,
    )

    # dP_dm_min = KDEUnivariate(m_min)
    # dP_dm_min.fit(kernel="gau", bw="scott", fft=False, weights=weights)
    # dP_dm_min_smooth = dP_dm_min.evaluate(m_min_smooth)

    dP_dm_min = gaussian_kde(
        m_min, weights=weights,
        bw_method="scott",
    )
    dP_dm_min_smooth = dP_dm_min(m_min_smooth)

    ax.plot(m_min_smooth, dP_dm_min_smooth)

    ax.set_xlabel(
        r"${m_min_label}$"
        .format(m_min_label=m_min_label)
    )
    ax.set_ylabel(
        r"$p({m_min_label})$"
        .format(m_min_label=m_min_label)
    )

    return "m_min"


def _plot_m_max(ax, m_max, weights, n_samples, metadata):
    import numpy
#    from statsmodels.nonparametric.kde import KDEUnivariate
    from ..utils import gaussian_kde

    from . import plot_config

    m_max_label = plot_config.labels["m_max"]

    m_max_smooth = numpy.linspace(
        metadata["m_max_min"], metadata["m_max_max"],
        n_samples,
    )

    # dP_dm_max = KDEUnivariate(m_max)
    # dP_dm_max.fit(kernel="gau", bw="scott", fft=False, weights=weights)
    # dP_dm_max_smooth = dP_dm_max.evaluate(m_max_smooth)

    dP_dm_max = gaussian_kde(
        m_max, weights=weights,
        bw_method="scott",
    )
    dP_dm_max_smooth = dP_dm_max(m_max_smooth)

    ax.plot(m_max_smooth, dP_dm_max_smooth)

    ax.set_xlabel(
        r"${m_max_label}$"
        .format(m_max_label=m_max_label)
    )
    ax.set_ylabel(
        r"$p({m_max_label})$"
        .format(m_max_label=m_max_label)
    )

    return "m_max"


if __name__ == "__main__":
    import sys
    raw_args = sys.argv[1:]
    sys.exit(main(raw_args))
