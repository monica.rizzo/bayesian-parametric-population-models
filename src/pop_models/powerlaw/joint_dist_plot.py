from __future__ import division, print_function


def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing MCMC samples.",
    )
    parser.add_argument(
        "plot",
        help="File to store plot in.",
    )

    parser.add_argument(
        "--n-samples",
        default=100, type=int,
        help="Number of plotting samples to use along each dimension.",
    )

    parser.add_argument(
        "--rate-logU-to-jeffereys",
        action="store_true",
        help="Assuming rate is log-uniform, switch to Jefferey's prior.",
    )

    parser.add_argument(
        "--plot-lim-alpha",
        type=float, default=None, nargs=2,
    )
    parser.add_argument(
        "--plot-lim-log-rate",
        type=float, default=None, nargs=2,
    )

    parser.add_argument(
        "--alpha-min",
        type=float, default=-2,
        help="Minimum alpha to plot.",
    )
    parser.add_argument(
        "--alpha-max",
        type=float, default=+7,
        help="Maximum alpha to plot.",
    )

    parser.add_argument(
        "--rate-min",
        type=float, default=1e-3,
        help="Minimum rate to plot.",
    )
    parser.add_argument(
        "--rate-max",
        type=float, default=1e+3,
        help="Maximum rate to plot.",
    )

    parser.add_argument(
        "--contour-levels",
        type=float, default=[0.5, 0.9], nargs="+",
    )
    parser.add_argument(
        "--cmap",
        default="magma_r",
        help="Matplotlib colormap to use for density plots.",
    )
    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib plots.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    if raw_args is None:
        import sys
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    import h5py
    import numpy

    from . import prob
    from . import plot_config
    from .. import utils
    from . import utils as pl_utils

    import matplotlib
    matplotlib.use(args.mpl_backend)
    import matplotlib.pyplot as plt

    import seaborn as sns

    matplotlib.rc("text", usetex=True)

    sns.set_context("talk", font_scale=2, rc={"lines.linewidth": 2.5})
    sns.set_style("ticks")
    sns.set_palette("colorblind")

    log10_rate_min = numpy.log10(args.rate_min)
    log10_rate_max = numpy.log10(args.rate_max)

    with h5py.File(args.posteriors, "r") as f:
        metadata = f.attrs
        param_names = prob.param_names
        params = pl_utils.load(f, param_names, metadata)

        log10_rates, alphas = pl_utils.upcast_scalars((
            params["log_rate"], params["alpha"],
        ))

        rates = numpy.power(10.0, log10_rates)

        if args.rate_logU_to_jeffereys:
            weights = numpy.sqrt(rates)
        else:
            weights = None

        alpha_smooth = numpy.linspace(
            metadata["alpha_min"], metadata["alpha_max"],
            args.n_samples,
        )
        log10_rate_smooth = numpy.linspace(
            metadata["log_rate_min"], metadata["log_rate_max"],
            args.n_samples,
        )

        alpha_grid, log10_rate_grid = numpy.meshgrid(
            alpha_smooth, log10_rate_smooth,
        )
        rate_grid = numpy.power(10.0, log10_rate_grid)

        alpha_smooth_samples = alpha_grid.ravel()
        log10_rate_smooth_samples = log10_rate_grid.ravel()

        kde = utils.gaussian_kde(
            [alphas, log10_rates],
            bw_method="scott", weights=weights,
        )
        dP_dalpha_dlog10_rate_grid = kde(
            [alpha_smooth_samples, log10_rate_smooth_samples],
        ).reshape(alpha_grid.shape)

        dP_dalpha_drate_grid = (
            dP_dalpha_dlog10_rate_grid / (rate_grid * numpy.log(10.0))
        )

        # kde = KDEMultivariate([alphas, rates])

#        hist, alpha_edges, rate_edges = numpy.histogram2d(
#            alphas, rates, weights=weights, bins=50,
#        )

#        log10_rate_edges = numpy.log10(rate_edges)

#        A_edges, LOGR_edges = numpy.meshgrid(alpha_edges, log10_rate_edges)


        # alpha_edges = numpy.linspace(
        #     args.alpha_min, args.alpha_max, args.n_samples,
        # )
        # log10_rate_edges = numpy.linspace(
        #     log10_rate_min, log10_rate_max, args.n_samples,
        # )

        fig, ax = plt.subplots()


        # _, _, _, hist = ax.hist2d(
        #     alphas, log10_rates,
        #     weights=weights,
        #     normed=True, norm=matplotlib.colors.LogNorm(),
        #     bins=[alpha_edges, log10_rate_edges],
        #     cmap="viridis",
        # )

        cn = ax.contourf(
            alpha_grid, log10_rate_grid,
            dP_dalpha_dlog10_rate_grid,
            30,
            cmap=args.cmap,
        )
        # levels = utils.contour_levels(
        #     dP_dalpha_dlog10_rate_grid, args.contour_levels,
        # )
        # fmt = {
        #     level: r"{pct} \%%".format(pct=int(100*p))
        #     for level, p in zip(levels, args.contour_levels)
        # }
        # cn_lab = ax.contour(
        #     alpha_grid, log10_rate_grid,
        #     dP_dalpha_dlog10_rate_grid,
        #     sorted(levels),
        #     colors="white", linestyles="--",
        # )
        # ax.clabel(
        #     cn_lab, sorted(levels),
        #     inline=True, fontsize=10, fmt=fmt,
        #     color="white", linestyle="--",
        # )

        # cbar = fig.colorbar(hist)
        cbar = fig.colorbar(cn)
        # cbar.add_lines(cn_lab)

        alpha_label = plot_config.labels["alpha"]
        rate_label = plot_config.labels["rate"]
        rate_units = plot_config.units["rate"]

        ax.set_xlabel(
            r"${alpha_label}$"
            .format(alpha_label=alpha_label)
        )
        ax.set_ylabel(
            r"$\log_{{10}}({rate_label} / {rate_units})$"
            .format(rate_label=rate_label, rate_units=rate_units)
        )

        cbar.set_label(
            r"$p({alpha_label}, {rate_label} \mid \mathbf{{d}})$"
            .format(alpha_label=alpha_label, rate_label=rate_label)
        )

#        ax.set_xlabel(r"$\alpha$")
#        ax.set_ylabel(r"$\log_{10} \mathcal{R}$ [Gpc$^{-3}$ yr$^{-1}$]")

        # cbar.set_label(
        #     r"$"
        #     r"\mathrm{d}P"
        #     r" / "
        #     r"\mathrm{d}(\log_{10}\mathcal{R})"
        #     r" "
        #     r"\mathrm{d}\alpha"
        #     r"$"
        # )

#        ax.pcolormesh(A_edges, LOGR_edges, hist.T, cmap="viridis")


#        ax.hist2d(alphas, rates, weights=weights, bins=20, normed=True)

        # ax.imshow(
        #     hist, interpolation="nearest", origin="low",
        #     extent=[
        #         alpha_edges[0], alpha_edges[-1],
        #         log10_rate_edges[0], log10_rate_edges[-1],
        #     ],
        #     cmap="viridis",
        # )

        if args.plot_lim_alpha is not None:
            ax.set_xlim(args.plot_lim_alpha)
        if args.plot_lim_log_rate is not None:
            ax.set_ylim(args.plot_lim_log_rate)
#        ax.set_yscale("log")

#        ax.set_xlim([args.alpha_min, args.alpha_max])
#        ax.set_ylim([args.rate_min, args.rate_max])
#        ax.set_ylim([log10_rate_min, log10_rate_max])

    fig.tight_layout()

    fig.savefig(args.plot)
