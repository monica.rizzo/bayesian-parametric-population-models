r"""
This module contains everything pertaining to the powerlaw mass distribution
model used in multiple LVC rate and population statements, such as the O1 BBH
paper [O1BBH]_ and the GW170104 detection paper [GW170104]_. It defines the
appropriate probability density functions in :mod:`.prob`, and in :mod:`.mcmc`
wraps the base :mod:`pop_models.mcmc` with an MCMC module that implements this
mass distribution model.

This model assumes that the primary mass, :math:`m_1`, is drawn from a power law
:math:`p(m_1) \propto m_1^{-\alpha}`, for some power law index :math:`\alpha`,
and bounded between some lower and upper limits, :math:`m_{\mathrm{min}}` and
:math:`m_{\mathrm{max}}` (Equation D1 in [O1BBH]_). Then it assumes that the
secondary mass, :math:`m_2`, is drawn from a uniform distribution between the
lower limit :math:`m_{\mathrm{min}}` and the primary mass :math:`m_1` (Equation
D2 in [O1BBH]_). Then the constraint :math:`m_1 + m_2 \leq M_{\mathrm{max}}` is
imposed, which turns :math:`p(m_1)` into a broken power law.

The joint PDF is given by

.. math::
   p(m_1, m_2) =
   C(\alpha, m_{\mathrm{min}}, m_{\mathrm{max}}, M_{\mathrm{max}}) \,
   \frac{m_1^{-\alpha}}{m_1 - m_{\mathrm{min}}}

and the marginal PDF for the primary mass is given by

.. math::
   p(m_1) =
   C(\alpha, m_{\mathrm{min}}, m_{\mathrm{max}}, M_{\mathrm{max}}) \,
   m_1^{-\alpha} \,
   \frac{\min(m_1, M_{\mathrm{max}}-m_1) - m_{\mathrm{min}}}
        {m_1 - m_{\mathrm{min}}}

where :math:`C(\alpha, m_{\mathrm{min}}, m_{\mathrm{max}}, M_{\mathrm{max}})` is
a normalization constant, whose value is derived in [T1700479]_.

.. note::
   Need to add citation for PDF functions. Current best citation is
   https://git.ligo.org/RatesAndPopulations/O2Populations/blob/master/powerlaw/PowerLaw.ipynb,
   which is not properly citable at the moment.

.. [O1BBH]
    Binary Black Hole Mergers in the First Advanced LIGO Observing Run,
    LIGO Scientific Collaboration and Virgo Collaboration,
    2016,
    Phys. Rev. X 6, 041015,
    DOI:
    `10.1103/PhysRevX.6.041015 <https://doi.org/10.1103/PhysRevX.6.041015>`_

.. [GW170104]
    GW170104: Observation of a 50-Solar-Mass Binary Black Hole Coalescence at
    Redshift 0.2,
    LIGO Scientific and Virgo Collaboration,
    2017,
    Phys. Rev. Lett. 118, 221101,
    DOI:
    `10.1103/PhysRevLett.118.221101 <https://doi.org/10.1103/PhysRevLett.118.221101>`_

.. [T1700479]
    Normalization constant in power law BBH mass distribution model,
    Daniel Wysocki and Richard O'Shaughnessy,
    `LIGO-T1700479 <https://dcc.ligo.org/LIGO-T1700479>`_
"""
from __future__ import absolute_import

from . import (
    mcmc,
    prob,
)
