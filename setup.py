"""Bayesian parametric population models (pop_models)

Long description goes here...
"""

from datetime import date


#-------------------------------------------------------------------------------
#   GENERAL
#-------------------------------------------------------------------------------
__name__        = "pop_models"
__version__     = "0.1.0a2"
__date__        = date(2017, 10, 18)
__keywords__    = [
    "astronomy",
    "information analysis",
    "machine learning",
    "physics",
]
__status__      = "Alpha"


#-------------------------------------------------------------------------------
#   URLS
#-------------------------------------------------------------------------------
__url__         = "https://git.ligo.org/daniel.wysocki/bayesian-parametric-population-models"
#__download_url__= "https://github.com/oshaughn/InferenceTools-LIGO-ScienceMode/releases/tag/{version}".format(version=__version__)
__bugtrack_url__= "https://git.ligo.org/daniel.wysocki/bayesian-parametric-population-models/issues"


#-------------------------------------------------------------------------------
#   PEOPLE
#-------------------------------------------------------------------------------
__author__      = "Daniel Wysocki and Richard O'Shaughnessy"
__author_email__= "daniel.wysocki@ligo.org"

__maintainer__      = "Daniel Wysocki"
__maintainer_email__= "daniel.wysocki@ligo.org"

__credits__     = ("Daniel Wysocki", "Richard O'Shaughnessy",)


#-------------------------------------------------------------------------------
#   LEGAL
#-------------------------------------------------------------------------------
__copyright__   = 'Copyright (c) 2017 {author} <{email}>'.format(
    author=__author__,
    email=__author_email__
)

__license__     = 'TBD'
__license_full__= '''
Licensed under ...TBD...
<link to license goes here>
'''.strip()


#-------------------------------------------------------------------------------
#   PACKAGE
#-------------------------------------------------------------------------------
DOCLINES = __doc__.split("\n")

CLASSIFIERS = """
Development Status :: 3 - Alpha
Programming Language :: Python
Programming Language :: Python :: 2
Programming Language :: Python :: 3
Operating System :: OS Independent
Intended Audience :: Science/Research
Topic :: Scientific/Engineering :: Astronomy
Topic :: Scientific/Engineering :: Physics
""".strip()

REQUIREMENTS = {
    "install": [
        "astropy>=1.3.0,<1.4.0",
#        "corner==2.0.1",
#        "coverage==4.3.1",
#        "cov-core==1.15.0",
        "emcee>=2.2.0,<2.3.0",
        "h5py>=2.7.0,<2.8.0",
        "mpmath>=1.0.0,<1.1.0",
        "numpy>=1.13.0,<1.14.0",
        "matplotlib>=2.0.0,<3.0.0",
        "pandas>=0.21.0,<0.22.0",
#        "preggy==1.3.0",
#        "scikit-learn==0.17.1",
        "scipy>=0.18.0,<0.19.0",
        "seaborn>=0.8.0,<0.9.0",
        "six>=1.10.0,<1.11.0",
#        "sympy>=1.1.0,<1.2.0",
#        "statsmodels>=0.8.0",
    ],
    "tests": [
    ]
}

ENTRYPOINTS = {
    "console_scripts" : [
        "pop_models_vt = pop_models.vt:_main",
        "pop_models_vt_plot = pop_models.vt:_main_plot",
        "pop_models_extract_samples = pop_models.extract_samples:_main",
        "pop_models_powerlaw_mcmc = pop_models.powerlaw.mcmc:_main",
        "pop_models_powerlaw_ci = pop_models.powerlaw.confidence_intervals:_main",
        "pop_models_powerlaw_marginal_plots = pop_models.powerlaw.marginal_plots:_main",
#        "pop_models_powerlaw_diagnostic_plots = pop_models.powerlaw.diagnostic_plots:_main",
#        "pop_models_powerlaw_debug_plots = pop_models.powerlaw.debug_plots:_main",
        "pop_models_powerlaw_dist_plots = pop_models.powerlaw.dist_plots:_main",
        "pop_models_powerlaw_joint_dist_plot = pop_models.powerlaw.joint_dist_plot:_main",
        "pop_models_powerlaw_spin_mag_mcmc = pop_models.powerlaw_spin_mag.mcmc:_main",
        "pop_models_powerlaw_spin_mag_marginal_plots = pop_models.powerlaw_spin_mag.marginal_plots:_main",
        "pop_models_powerlaw_spin_mag_dist_plots = pop_models.powerlaw_spin_mag.dist_plots:_main",
        "pop_models_powerlaw_spin_vec_mcmc = pop_models.powerlaw_spin_vec.mcmc:_main",
        "pop_models_powerlaw_spin_vec_marginal_plots = pop_models.powerlaw_spin_vec.marginal_plots:_main",
        "pop_models_powerlaw_spin_vec_dist_plots = pop_models.powerlaw_spin_vec.dist_plots:_main",
    ]
}

from setuptools import find_packages, setup

metadata = dict(
    name        =__name__,
    version     =__version__,
    description =DOCLINES[0],
    long_description='\n'.join(DOCLINES[2:]),
    keywords    =__keywords__,

    author      =__author__,
    author_email=__author_email__,

    maintainer  =__maintainer__,
    maintainer_email=__maintainer_email__,

    url         =__url__,
#    download_url=__download_url__,

    license     =__license__,

    classifiers=[f for f in CLASSIFIERS.split('\n') if f],

    package_dir ={"": "src"},
    packages    =[
        "pop_models",
        "pop_models.powerlaw",
    ],
#    packages=find_packages(exclude=['tests', 'tests.*']),

    install_requires=REQUIREMENTS["install"],
#    tests_require=REQUIREMENTS["tests"],

    entry_points=ENTRYPOINTS
)

setup(**metadata)
